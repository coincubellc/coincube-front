// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint'
  },
  env: {
    browser: true,
  },
  // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
  extends: [
    // it is important for airbnb to be listed first so that it can be overridden by prettier.
    'airbnb-base',
    // vue rule options from least strict to most strict are:
    // `plugin:vue/essential`, `plugin:vue/strongly-recommended`, `plugin:vue/recommended`
    'plugin:vue/recommended',
    // note that at this time, vue-loader must be version 14.2.3 and prettier
    // must be version 1.12.1 or prettier will not work with vue.
    // https://github.com/prettier/eslint-config-prettier
    // prettier editor integration: https://prettier.io/docs/en/editors.html
    'plugin:prettier/recommended'
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // check if imports actually resolve
  settings: {
    'import/resolver': {
      webpack: {
        config: 'build/webpack.base.conf.js'
      }
    }
  },
  // add your custom rules here
  rules: {
    "no-console": 'off',
    "no-plusplus": 'off',
    "linebreak-style": 'off',
    "arrow-parens": [2, "as-needed"],
    // don't require .vue extension when importing
    'import/extensions': ['error', 'always', {
      js: 'never',
      vue: 'never'
    }],
    // disallow reassignment of function parameters
    // disallow parameter object manipulation except for specific exclusions
    'no-param-reassign': ['error', {
      props: true,
      ignorePropertyModificationsFor: [
        'state', // for vuex state
        'acc', // for reduce accumulators
        'e' // for e.returnvalue
      ]
    }],
    // allow optionalDependencies
    'import/no-extraneous-dependencies': ['error', {
      optionalDependencies: ['test/unit/index.js']
    }],
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
}
