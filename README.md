# coincube-front

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Prettier

We are using the eslint prettier plugin for code formatting. When you run dev, eslint will throw 
errors on prettier issues as well as on linting issues. Unfortunately the auto-fix feature was 
broken. So if you want to auto-format a big block of code rather than fix issues one by one, you 
will need to configure your IDE's prettier settings (if available). Instructions for that can be 
found here: https://prettier.io/docs/en/editors.html
