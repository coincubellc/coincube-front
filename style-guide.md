# Style Guides

This document enlist all the style guides that anyone writing frontend code for Coincube has to follow.

# HTML & CSS

- http://codeguide.co/

## Sass/SCSS

- https://css-tricks.com/sass-style-guide/

# JavaScript

- https://github.com/airbnb/javascript

## Vue

- https://vuejs.org/v2/style-guide/

Also as general rules -- taken from the Zen of Python -- these should prevail above personal preferences.

- Explicit is better than implicit.
- Readability counts.
