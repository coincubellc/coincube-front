const account = require('../pages/account');
const home = require('../pages/home');
const sidenav = require('../pages/sidenav');
const cubes = require('../pages/cubes');

module.exports = {
  '@disabled': false,
  $email: null,
  'Open New Cube (Index)': function newCube(browser) {
    this.$email = browser.globals.email();

    home(browser).signupLogin(this.$email);

    sidenav(browser).navToNewCube();

    cubes(browser)
      .addNewCube()
      .navBackToCube()
      .verifyCubeWasAdded();
  },
  'Delete Cube': function cubeDelete(browser) {
    cubes(browser)
      .openSettings()
      .deleteCube()
      .verifyCubeWasDelete('Bitfinex');

    account(browser).delete();
  },
  'Cube Algorithm Instructions Modal': function instructions(browser) {
    this.$email = browser.globals.email();

    home(browser).signupLogin(this.$email);

    sidenav(browser).navToNewCube();

    cubes(browser)
      .addNewCube()
      .openInstructions();

    account(browser).delete();
  },
  'Switch to Index Algo': function switchToIndex(browser) {
    home(browser).loginBasicAccount();

    sidenav(browser).navToNewCube();
    cubes(browser)
      .addNewCube()
      .selectAlgorithm('Index')
      .navBackToCube()
      .openSettings()
      .deleteCube();

    browser.end();
  },
  'Switch To Centaur Algo': function switchToCentaur(browser) {
    home(browser).loginProAccount();

    sidenav(browser).navToNewCube();

    cubes(browser)
      .addNewCube()
      .selectAlgorithm('Centaur')
      .navBackToCube();
  },
  'Turn Auto Rebalance off': function autoRebalance(browser) {
    cubes(browser)
      .openSettings()
      .turnAutoRebalanceOff();
  },
  'Turn Auto Rebalance On': function autoRebalanceOn(browser) {
    cubes(browser).turnAutoRebalanceOn();
  },
  'Clean up pro account': function cleanup(browser) {
    cubes(browser).deleteCube();
  },
};
