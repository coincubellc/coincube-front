const account = require('../pages/account');
const home = require('../pages/home');
const sidenav = require('../pages/sidenav');
// const dashboard = require('../pages/dashboard');
// const cubes = require('../pages/cubes');

module.exports = {
  '@disabled': false,
  $email: null,
  beforeEach(browser) {
    this.$email = browser.globals.email();

    home(browser).signupLogin(this.$email);
  },
  afterEach(browser) {
    account(browser).delete();
  },
  'Account - Update Email': function accountEmail(browser) {
    account(browser)
      .navToEmailSettings()
      .verifyCurrentEmail(this.$email);

    // generate a new email
    this.$email = browser.globals.email();

    account(browser).updateEmail(this.$email);

    home(browser).login(this.$email);

    account(browser)
      .navToEmailSettings()
      .verifyCurrentEmail(this.$email);

    sidenav(browser).logout();
    home(browser).login(this.$email);
  },
  'Account - Update Password': function accountEmail(browser) {
    account(browser).navToPasswordSettings();
    account(browser).updatePassword(1);

    sidenav(browser).logout();
    home(browser).login(this.$email, 1);
  },
  // Commenting out as showPerformances is returning undefined
  // 'Account - Display Bitcoin Performance Data': function performanceData(
  //   browser,
  // ) {
  //   sidenav(browser).navToDashboard();
  //   dashboard(browser).verifyPerformanceDataNotVisible();

  //   sidenav(browser).navToNewCube();
  //   cubes(browser).addNewCube();

  //   account(browser).navToSettings();
  //   account(browser).toggleBitcoinPerformanceData();

  //   sidenav(browser).navToDashboard();
  //   dashboard(browser).verifyPerformanceDataVisible();

  //   account(browser).navToSettings();
  //   account(browser).toggleBitcoinPerformanceData();

  //   sidenav(browser).navToDashboard();
  //   dashboard(browser).verifyPerformanceDataNotVisible();
  // }
};
