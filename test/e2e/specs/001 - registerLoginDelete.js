const account = require('../pages/account');
const home = require('../pages/home');

module.exports = {
  '@disabled': false,
  $email: null,
  before(browser) {
    this.$email = browser.globals.email();
  },
  after(browser) {
    account(browser).delete();
  },
  'Register E2E tests': function loginTest(browser) {
    home(browser).signupLogin(this.$email);
  },
  'Verify new accounts show dashboard': function dashboard(browser) {
    account(browser).verifyDashboard();
  },
};
