const home = require('../pages/home');
const sidenav = require('../pages/sidenav');
const account = require('../pages/account');
const wallet = require('../pages/wallet');

module.exports = {
  '@disabled': false,
  $email: null,
  before(browser) {
    this.$email = browser.globals.email();

    home(browser).signupLogin(this.$email);
  },
  after(browser) {
    account(browser).delete();
  },
  'Navigate to new wallet': function newWallet(browser) {
    sidenav(browser).navToNewWallet();
  },
  'Add a wallet': function addWallet(browser) {
    wallet(browser).addWallet();
  },
  'Show Instructions': function showInstructions(browser) {
    wallet(browser).viewWalletInstructions();
  },
  'Add an address to wallet': function addAddress(browser) {
    wallet(browser).addAddress();
  },
  'Open Wallet Settings': function openSettings(browser) {
    wallet(browser).openSettings();
  },
  'Delete Wallet': function deleteWallet(browser) {
    wallet(browser).deleteWallet();
  },
};
