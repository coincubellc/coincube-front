require('babel-register')();
const seleniumServer = require('selenium-server');
const chromedriver = require('chromedriver');
const config = require('../../config');
const uuidv1 = require('uuid/v1');

// http://nightwatchjs.org/gettingstarted#settings-file
module.exports = {
  src_folders: ['test/e2e/specs'],
  output_folder: 'test/e2e/reports',

  selenium: {
    start_process: true,
    server_path: seleniumServer.path,
    host: '127.0.0.1',
    port: 4444,
    cli_args: {
      'webdriver.chrome.driver': chromedriver.path,
    }
  },

  test_workers: { enabled: true, workers: 'auto' },

  test_settings: {
    default: {
      selenium_port: 4444,
      selenium_host: 'localhost',
      silent: true,
      request_timeout_options: {
        timeout: 60000,
        retry_attempts: 5,
      },
      globals: {
        devServerURL: `http://localhost:${process.env.PORT || config.dev.port}`,
        email() {
          return `test-e2e-account-${uuidv1()}@coincube.io`;
        },
        password(increment = '') {
          return `h5&RV5a$3Ll9Gpcn${increment}`;
        },
      },
    },

    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        chromeOptions: {
          args: ['start-maximized'],
        }
      }
    },

    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    }
  }
}
