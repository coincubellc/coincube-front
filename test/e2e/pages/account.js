module.exports = browser => ({
  delete() {
    this.navToSettings(browser);

    browser
      .waitForElementVisible('.nw-account-portfolio')
      .click('.nw-account-portfolio')
      .waitForElementVisible('.nw-account-delete-account')
      .click('.nw-account-delete-account')
      .pause(1000)
      .assert.urlEquals(`${browser.globals.devServerURL}/`)
      .assert.elementPresent('.nw-login')
      .end();
    return this;
  },
  verifyDashboard() {
    browser.waitForElementVisible('.welcome');

    return this;
  },
  navToSettings() {
    browser
      .waitForElementVisible('.nw-settings-cog')
      .click('.nw-settings-cog')
      .assert.urlEquals(`${browser.globals.devServerURL}/account/settings`)
      .waitForElementVisible('.account-settings');
    return this;
  },
  navToEmailSettings() {
    this.navToSettings(browser);
    browser
      .waitForElementVisible('.nw-account-email')
      .click('.nw-account-email')
      .waitForElementVisible('.settings-email')
      .pause(1000);
    return this;
  },
  navToPasswordSettings() {
    this.navToSettings(browser);

    browser
      .waitForElementVisible('.nw-account-password')
      .click('.nw-account-password')
      .waitForElementVisible('.settings-password')
      .pause(1000);
    return this;
  },
  toggleBitcoinPerformanceData() {
    browser
      .waitForElementVisible('.nw-btc-performance-toggle')
      .click('.nw-btc-performance-toggle')
      .pause(1000);
    return this;
  },
  verifyCurrentEmail(email) {
    browser
      .waitForElementVisible('.nw-account-email-current-address')
      .assert.containsText('.nw-account-email-current-address', email);
    return this;
  },
  updateEmail(email, increment = '') {
    // pause at the end -> hack to get around async issue on account email page
    browser
      .setValue('.nw-account-email-input input', email)
      .setValue(
        '.nw-account-email-password-input input',
        browser.globals.password(increment),
      )
      .click('.nw-account-email-submit')
      .waitForElementVisible('.toasted.primary.success')
      .pause(2000);
    return this;
  },
  updatePassword(increment = '') {
    browser
      .setValue('.nw-account-password-current', browser.globals.password())
      .setValue('.nw-account-password-new', browser.globals.password(increment))
      .setValue(
        '.nw-account-password-new-confirm',
        browser.globals.password(increment),
      )
      .pause(1000)
      .click('.nw-account-password-submit')
      .waitForElementVisible('.toasted.primary.success');
    return this;
  },
});
