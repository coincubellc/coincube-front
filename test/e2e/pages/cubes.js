const sidenav = require('./sidenav');

module.exports = browser => ({
  addNewCube(
    cubeName,
    key = 'WLLiKfuGK8Y1867UX4BWkS8sYqI36rlYY6QylsBzMtU',
    secret = '8zR7VXynemCNE9rr3ZSeK75gUTaPwxTNocrgaKOWV5E',
  ) {
    browser
      .waitForElementVisible('.nw-connection-card-Bitfinex-connect')
      .click('.nw-connection-card-Bitfinex-connect')
      .waitForElementVisible('.nw-modal')
      .setValue('.nw-api-key input', key)
      .setValue('.nw-api-secret input', secret)
      .pause(1000)
      .click('.nw-submit')
      .waitForElementVisible('.algorithms.container', 200000)
      .assert.urlContains('/algorithm');

    return this;
  },
  navBackToCube() {
    browser
      .click('.back-to-cube-btn')
      .waitForElementVisible('.nw-cube-name', 10000)
      .assert.urlContains('/account/cube/');
    return this;
  },
  verifyCubeWasAdded(cubeName = 'Bitfinex') {
    browser.assert
      .containsText('.nw-cube-name', `Cube: ${cubeName}`)
      .pause(1000);
    return this;
  },
  openSettings() {
    browser
      .waitForElementVisible('.nw-cube-settings')
      .click('.nw-cube-settings')
      .pause(1000);
    return this;
  },
  deleteCube() {
    browser
      .waitForElementVisible('.nw-cube-settings-delete')
      .click('.nw-cube-settings-delete')
      .pause(1000)
      .refresh();

    return this;
  },
  turnAutoRebalanceOff() {
    browser
      .waitForElementPresent('.modal-container input')
      .assert.containsText('.modal-container label', 'On')
      .click('.modal-container .switch-accent')
      .pause(1000)
      .assert.containsText('.modal-container label', 'Off')
      .assert.hidden('.modal-container .centaur-controls');

    return this;
  },
  turnAutoRebalanceOn() {
    browser
      .waitForElementPresent('.modal-container input')
      .assert.containsText('.modal-container label', 'Off')
      .click('.modal-container .switch-accent')
      .pause(1000)
      .assert.containsText('.modal-container label', 'On')
      .assert.visible('.modal-container .centaur-controls');

    return this;
  },
  verifyCubeWasDelete(cubeName) {
    sidenav(browser).openAccordion('Cubes');

    browser.assert.elementNotPresent(
      `.nw-sidenav-cubes .nw-sidenav-${cubeName}`,
    );

    return this;
  },
  toggleBTCFiat() {
    browser.assert
      .urlContains('/account/cube/')
      .waitForElementVisible('.right.switch')
      .assert.containsText('.table th:nth-child(3) span', 'BTC Rate')
      .click('.right.switch')
      .pause(500)
      .assert.containsText('.table th:nth-child(3) span', 'Fiat Rate')
      .click('.right.switch')
      .pause(500)
      .assert.containsText('.table th:nth-child(3) span', 'BTC Rate');

    return this;
  },
  clickSelectAlgorithm() {
    const selector = '.nw-cube-select-algorithm-btn';

    browser.assert
      .urlContains('/account/cube/')
      .waitForElementPresent(selector)
      .moveToElement(selector)
      .waitForElementVisible(selector)
      .click(selector)
      .waitForElementPresent('.algorithms')
      .assert.urlContains('/algorithm');

    return this;
  },
  clickPortfolioGenerator() {
    const selector = 'nw-cube-portfolio-generator-btn';

    browser.assert
      .urlContains('/account/cube/')
      .waitForElementPresent(selector)
      .moveToElement(selector)
      .waitForElementVisible(selector)
      .click(selector)
      .waitForElementPresent('.generator')
      .assert.urlContains('/generator');

    return this;
  },
  openInstructions() {
    browser
      .waitForElementVisible('.nw-algo-instructions')
      .click('.nw-algo-instructions button')
      .waitForElementVisible('.modal-container')
      .click('.close')
      .pause(1000);

    return this;
  },
  selectAlgorithm(algo = 'Tracker') {
    browser
      .waitForElementVisible(`.nw-square-${algo}`)
      .click(`.nw-square-${algo}`);

    if (algo !== 'Tracker') {
      browser.pause(1000).waitForElementVisible('.port-gen-btn');
    }

    browser.pause(2000).assert.elementNotPresent('.error');

    return this;
  },
});
