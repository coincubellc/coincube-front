const account = require('./account');

module.exports = browser => ({
  logout() {
    browser.assert
      .elementPresent('.nw-sidenav-logout')
      .click('.nw-sidenav-logout')
      .pause(1000)
      .assert.urlEquals(`${browser.globals.devServerURL}/`);

    return this;
  },
  openAccordion(menuItem) {
    account(browser).navToSettings();

    browser.click(`.nw-${menuItem}-accordion`).pause(1000);

    return this;
  },
  navToNewCube() {
    this.openAccordion('Cubes');

    browser
      .waitForElementVisible('.nw-sidenav-add-cube')
      .click('.nw-sidenav-add-cube')
      .assert.urlEquals(
        `${browser.globals.devServerURL}/account/cube/add/connection`,
      )
      .waitForElementVisible('.connections');

    return this;
  },
  navToCube(cubeName) {
    this.openAccordion('Cubes');

    browser
      .waitForElementVisible(`.nw-sidenav-Cubes .nw-sidenav-${cubeName}`)
      .click('.nw-sidenav-Cubes .nw-sidenav-Bitfinex')
      .pause(1000)
      .assert.urlContains('account/cube');

    return this;
  },
  navToDashboard() {
    browser
      .waitForElementVisible('.nw-sidenav-Dashboard')
      .click('.nw-sidenav-Dashboard')
      .pause(1000)
      .assert.urlEquals(`${browser.globals.devServerURL}/account`);

    return this;
  },
  navToNewWallet() {
    this.openAccordion('Wallets');

    browser
      .waitForElementVisible('.nw-sidenav-add-wallet')
      .click('.nw-sidenav-add-wallet')
      .assert.urlEquals(`${browser.globals.devServerURL}/account/addwallet`)
      .waitForElementVisible('.nw-addwallet');

    return this;
  },
});
