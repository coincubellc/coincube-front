module.exports = browser => ({
  signup(email, increment = '') {
    browser
      .url(browser.globals.devServerURL)
      .waitForElementVisible('.navbar', 5000)
      .click('.nw-register')
      .pause(1000)
      .assert.urlEquals(`${browser.globals.devServerURL}/signup`)
      .setValue('.nw-register-name input[type=text]', 'CoincubeNWTestAccount')
      .setValue('.nw-register-email input[type=text]', email)
      .setValue(
        '.nw-register-password input[type=password]',
        browser.globals.password(increment),
      )
      .setValue(
        '.nw-register-password-confirm input[type=password]',
        browser.globals.password(increment),
      )
      .click('.nw-register-signup')
      .waitForElementVisible('.toasted.primary.info')
      .waitForElementVisible('.toasted.primary.success')
      .assert.containsText('.toasted.primary.success', 'Signed up!')
      .assert.urlEquals(
        `${browser.globals.devServerURL}/unconfirmed/CoincubeNWTestAccount`,
      )
      .waitForElementVisible('.nw-register-confirmation');

    return this;
  },
  login(email, increment = '') {
    browser.assert
      .elementPresent('.nw-login')
      .click('.nw-login')
      .waitForElementVisible('.nw-login-modal')
      .setValue('.nw-login-email input[type=email]', email)
      .setValue(
        '.nw-login-password input[type=password]',
        browser.globals.password(increment),
      )
      .click('.nw-login-button')
      .waitForElementVisible('.toasted.primary.success')
      .waitForElementVisible('.side-nav')
      .assert.urlEquals(`${browser.globals.devServerURL}/account`);

    return this;
  },
  signupLogin(email, increment = '') {
    this.signup(email, increment);
    this.login(email);

    return this;
  },
  loginBasicAccount() {
    browser
      .url(browser.globals.devServerURL)
      .waitForElementVisible('.navbar', 5000)
      .assert.elementPresent('.nw-login')
      .click('.nw-login')
      .waitForElementVisible('.nw-login-modal')
      .setValue('.nw-login-email input[type=email]', 'test_basic@test.com')
      .setValue('.nw-login-password input[type=password]', 'Bitcoin1')
      .click('.nw-login-button')
      .waitForElementVisible('.toasted.primary.success')
      .waitForElementVisible('.side-nav')
      .assert.urlEquals(`${browser.globals.devServerURL}/account`);

    return this;
  },
  loginProAccount() {
    browser
      .url(browser.globals.devServerURL)
      .waitForElementVisible('.navbar', 5000)
      .assert.elementPresent('.nw-login')
      .click('.nw-login')
      .waitForElementVisible('.nw-login-modal')
      .setValue('.nw-login-email input[type=email]', 'test_pro@test.com')
      .setValue('.nw-login-password input[type=password]', 'Bitcoin1')
      .click('.nw-login-button')
      .waitForElementVisible('.toasted.primary.success')
      .waitForElementVisible('.side-nav')
      .assert.urlEquals(`${browser.globals.devServerURL}/account`);
  }
});
