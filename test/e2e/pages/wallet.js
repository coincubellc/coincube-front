module.exports = browser => ({
  addWallet() {
    browser
      .waitForElementVisible('.nw-addwallet')
      .setValue('input', 'testWallet')
      .pause(1000)
      .click('.nw-addwallet button')
      .pause(2000);

    return this;
  },
  viewWalletInstructions() {
    browser
      .waitForElementVisible('.instruction-component')
      .click('.instruction-component button:first-child')
      .waitForElementVisible('.modal-container')
      .click('a.close')
      .pause(1000);

    return this;
  },
  addAddress() {
    browser
      .waitForElementVisible('.instruction-component')
      .click('.instruction-component button:last-child')
      .waitForElementVisible('.modal-container')
      .click('option[value="Bitcoin"]')
      .pause(500)
      .setValue('.modal-container input', '1CK6KHY6MHgYvmRQ4PAafKYDrg1ejbH1cE')
      .pause(500)
      .click('.modal-container button')
      .waitForElementVisible('.toasted.primary.success', 10000)
      .waitForElementVisible('.nw-balances-table', 10000)
      .waitForElementVisible('.nw-transactions-table');

    return this;
  },
  openSettings() {
    browser
      .waitForElementVisible('.nw-wallet-settings-button')
      .click('.nw-wallet-settings-button')
      .waitForElementVisible('.nw-wallet-settings-modal');

    return this;
  },
  deleteWallet() {
    browser
      .waitForElementVisible('.nw-wallet-settings-modal')
      .click('.nw-delete-wallet')
      .waitForElementVisible('.toasted.primary.info')
      .waitForElementNotPresent('.toasted.primary.info', 10000)
      .assert.urlEquals(`${browser.globals.devServerURL}/account`);

    return this;
  },
});
