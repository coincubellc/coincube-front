module.exports = browser => ({
  verifyPerformanceDataVisible() {
    browser
      .waitForElementVisible('.container')
      .assert.elementPresent('.nw-account-performance div:nth-child(3)')
      .assert.elementPresent('.nw-account-performance div:nth-child(4)');

    return this;
  },
  verifyPerformanceDataNotVisible() {
    browser
      .waitForElementVisible('.container')
      .assert.elementNotPresent('.nw-account-performance div:nth-child(3)')
      .assert.elementNotPresent('.nw-account-performance div:nth-child(4)');
    return this;
  },
});
