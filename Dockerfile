FROM node:8.9 as build

ARG PRIV_KEY=null
WORKDIR /usr/app

RUN mkdir -p /root/.ssh
RUN echo ${PRIV_KEY} | base64 -d > /root/.ssh/id_rsa
RUN chmod 400 /root/.ssh/id_rsa
RUN echo "Host gitlab.com\n\tStrictHostKeyChecking no\n\tIdentityFile /root/.ssh/id_rsa\n" >> /root/.ssh/config

COPY . .
RUN npm rebuild node-sass
RUN npm install
RUN npm run build

RUN rm -rf /root/.ssh

FROM nginx:1.15

COPY --from=build /usr/app/dist /usr/share/nginx/html
WORKDIR /usr/share/nginx/html
COPY cicd/nginx/default.conf.template /etc/nginx/conf.d/default.conf.template
COPY cicd/docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]