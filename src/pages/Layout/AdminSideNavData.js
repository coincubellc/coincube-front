import BillingIcon from '@/assets/icons/icons8-billing.svg';
import DashboardIcon from '@/assets/icons/icons8-dashboard.svg';
import GearIcon from '@/assets/icons/icons8-gear.svg';
import LogoutIcon from '@/assets/icons/icons8-logout.svg';
import CubesIcon from '@/assets/icons/icons8-cube.svg';

export default (user, _this) => [
  {
    route: '/admin',
    label: 'Dashboard',
    icon: DashboardIcon,
    iconFa: 'fas fa-tachometer-alt',
  },
  {
    route: '/admin/cubes',
    label: 'Cubes',
    icon: CubesIcon,
  },
  {
    route: '/admin/billing',
    label: 'Billing',
    icon: BillingIcon,
  },
  {
    route: '/admin/settings',
    label: 'Settings',
    icon: GearIcon,
    iconFa: 'fas fa-question-circle',
  },
  {
    label: 'Log out',
    icon: LogoutIcon,
    iconFa: 'fas fa-sign-out-alt',
    class: 'nw-sidenav-logout',
    action: () => _this.$auth.signOut(),
  },
];
