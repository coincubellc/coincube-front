import BillingIcon from '@/assets/icons/icons8-billing.svg';
import CubesIcon from '@/assets/icons/icons8-cube.svg';
import DashboardIcon from '@/assets/icons/icons8-dashboard.svg';
import SupportIcon from '@/assets/icons/icons8-support.svg';
import LogoutIcon from '@/assets/icons/icons8-logout.svg';
import PlusIcon from '@/assets/icons/icons8-plus.svg';
import RadarIcon from '@/assets/icons/icons8-radar.svg';
import GearIcon from '@/assets/icons/icons8-gear.svg';
import CentaurIcon from '@/assets/icons/centaur.svg';
import SphinxIcon from '@/assets/icons/sphinx.svg';
import ChipIcon from '@/assets/icons/icons8-risk-optimized.svg';
import featureFlags from 'utils/featureFlags';

const cubesIconMap = {
  Tracker: RadarIcon,
  Centaur: CentaurIcon,
  Index: GearIcon,
  Sphinx: SphinxIcon,
  Optimized: ChipIcon,
};

export default (user, _this) => {
  const hasCubes = user.open_cubes.length > 0;
  const cubeLinks = hasCubes
    ? user.open_cubes.map(cube => ({
        route: `/account/cube/${cube.id}`,
        label: cube.api_connections[0].exchange.name,
        icon: cubesIconMap[cube.algorithm.name],
      }))
    : [];
  return [
    {
      route: '/account',
      label: 'Dashboard',
      icon: DashboardIcon,
      iconFa: 'fas fa-tachometer-alt',
    },
    {
      children: cubeLinks,
      cta: {
        label: 'Add Cube',
        class: 'nw-sidenav-add-cube',
        route: '/account/cube/add/connection',
        icon: PlusIcon,
        iconFa: 'fas fa-plus',
      },
      label: 'Cubes',
      icon: CubesIcon,
      iconFa: 'fas fa-cube',
    },
    ...(featureFlags.enableBilling && [
      {
        route: '/account/billing',
        label: 'Billing',
        icon: BillingIcon,
      },
    ]),
    {
      route: '/account/support',
      label: 'Support',
      icon: SupportIcon,
      iconFa: 'fas fa-question-circle',
    },
    {
      label: 'Log out',
      icon: LogoutIcon,
      iconFa: 'fas fa-sign-out-alt',
      class: 'nw-sidenav-logout',
      action: () => _this.$auth.signOut(),
    },
  ];
};
