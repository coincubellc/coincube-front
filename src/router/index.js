import Vue from 'vue';
import VueRouter from 'vue-router';
import Assets from 'pages/Assets';
import Modes from 'pages/Modes';
import Indexes from 'pages/Indexes';
import SignUp from 'pages/Auth/SignUp';
import SignUpConfirmation from 'pages/Auth/SignUpConfirmation';
import EmailConfirmation from 'pages/EmailConfirmation';
import ResetPassword from 'pages/ResetPassword';
import Team from 'pages/Team';
import FAQPage from 'pages/FAQPage';
import Landing from 'pages/Landing';
import Account from 'pages/Account';
import Admin from 'pages/Admin';
import AdminBilling from 'pages/AdminBilling';
import CubeStats from 'pages/CubeStats';
import AccountSettings from 'pages/Settings';
import Cube from 'pages/Cube';
import Algorithm from 'pages/Algorithm';
import Wallet from 'pages/Wallet';
import WalletAdd from 'pages/Wallet/WalletAdd';
import OauthCallback from 'pages/Auth/OauthCallback';
import LandingPageLayout from '@/layouts/LandingPageLayout';
import AccountPageLayout from '@/layouts/AccountPageLayout';
import AdminPageLayout from 'layouts/AdminPageLayout';
import Connection from 'pages/Connection';
import Support from 'pages/Support/Support';
import Billing from 'pages/Billing';
import Generator from 'pages/Generator';
import notFound from 'pages/Error/notFound';
import notAllowed from 'pages/Error/403';
import internalError from 'pages/Error/500';
import featureFlags from 'utils/featureFlags';
import store from 'store/store';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: LandingPageLayout,
      meta: { title: 'COINCUBE | Blockchain Wealth Management' },
      children: [
        {
          name: 'Layout',
          path: '',
          component: Landing,
          meta: { title: 'COINCUBE | Blockchain Wealth Management' },
        },
        {
          path: '/modes',
          name: 'Modes',
          component: Modes,
          meta: { title: 'COINCUBE | Modes' },
        },
        {
          path: '/indexes',
          name: 'Indexes',
          component: Indexes,
          meta: { title: 'COINCUBE | Indexes' },
        },
        {
          path: '/assets',
          name: 'Assets',
          component: Assets,
          meta: { title: 'COINCUBE | Assets' },
        },
        {
          path: '/team',
          name: 'Team',
          component: Team,
          meta: { title: 'COINCUBE | Team' },
        },
        {
          path: '/faq',
          name: 'Faq',
          component: FAQPage,
          meta: { title: 'COINCUBE | FAQ' },
        },
        ...(featureFlags.enableSignup && [
          {
            path: '/signup',
            name: 'SignUp',
            component: SignUp,
            meta: {
              auth: false,
              meta: { title: 'COINCUBE | Sign Up' },
            },
          },
        ]),
        {
          path: '/confirm/:token',
          name: 'Email Confirmation',
          component: EmailConfirmation,
          meta: { title: 'COINCUBE | Email Confirmation' },
        },
        {
          path: '/unconfirmed/:username',
          name: 'Unconfirmed',
          component: SignUpConfirmation,
          meta: { title: 'COINCUBE | Sign Up Confirmation' },
        },
        {
          path: '/reset_password/:token',
          name: 'Reset Password',
          component: ResetPassword,
          meta: { title: 'COINCUBE | Reset Password' },
        },
        {
          name: 'Oauth Callback',
          path: '/oauth/callback',
          component: OauthCallback,
          meta: { title: 'COINCUBE | OAuth' },
        },
        {
          name: 'Not Found',
          path: '/notFound',
          component: notFound,
          meta: { title: 'COINCUBE | Not Found' },
        },
        {
          name: 'Not Allowed',
          path: '/notAllowed',
          component: notAllowed,
          meta: { title: 'COINCUBE | Not Allowed' },
        },
        {
          name: 'Internal Error',
          path: '/internalError',
          component: internalError,
          meta: { title: 'COINCUBE | Internal Error' },
        },
      ],
    },
    {
      path: '/account',
      component: AccountPageLayout,
      meta: {
        auth: true,
        title: 'COINCUBE | Account',
      },
      children: [
        {
          path: '',
          name: 'Account',
          component: Account,
          meta: { title: 'COINCUBE | Account' },
        },
        {
          path: 'settings',
          name: 'Settings',
          component: AccountSettings,
          meta: { title: 'COINCUBE | Settings' },
        },
        {
          path: 'support',
          name: 'Support',
          component: Support,
          meta: { title: 'COINCUBE | Support' },
        },
        {
          path: 'cube/:cubeID',
          name: 'Cube',
          component: Cube,
          meta: { title: 'COINCUBE | Cube' },
        },
        {
          path: 'cube/:cubeID/mode',
          name: 'Mode',
          component: Algorithm,
          meta: { title: 'COINCUBE | Mode' },
        },
        {
          path: 'cube/:cubeID/generator',
          name: 'Generator',
          component: Generator,
          meta: { title: 'COINCUBE | Generator' },
        },
        {
          path: 'cube/add/connection',
          name: 'Connection',
          component: Connection,
          meta: { title: 'COINCUBE | Connection' },
        },
        {
          path: 'wallet/:walletID',
          name: 'Wallet',
          component: Wallet,
          meta: { title: 'COINCUBE | Wallet' },
        },
        {
          path: 'addwallet',
          name: 'Add Wallet',
          component: WalletAdd,
          meta: { title: 'COINCUBE | Add Wallet' },
        },
        ...(featureFlags.enableBilling && [
          {
            path: 'billing',
            name: 'Billing',
            component: Billing,
            meta: { title: 'COINCUBE | Billing' },
          },
        ]),
      ],
    },
    {
      path: '/admin',
      component: AdminPageLayout,
      meta: {
        auth: true,
        title: 'COINCUBE | Admin',
      },
      children: [
        {
          path: '',
          name: 'Admin',
          component: Admin,
          meta: { title: 'COINCUBE | Admin' },
        },
        {
          path: 'cubes',
          name: 'Cubes',
          component: CubeStats,
          meta: { title: 'COINCUBE | Admin | Cubes' },
        },
        {
          path: 'admin billing',
          name: 'Admin Billing',
          component: AdminBilling,
          meta: { title: 'COINCUBE | Admin | Billing' },
        },
      ],
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => {
  if (!to.matched.length) {
    next('/notFound');
  } else {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    const nearestWithTitle = to.matched
      .slice()
      .reverse()
      .find(route => route.meta && route.meta.title);

    if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

    next();
  }
});

router.beforeEach((to, from, next) => {
  if (to.path !== from.path) {
    store.commit('showSpinner', true);
  }
  next();
});

router.afterEach(() => {
  Vue.nextTick(() => {
    store.commit('showSpinner', false);
  });
});

export default router;
