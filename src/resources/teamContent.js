// TODO: Remove file once db content is finished
const content = {
  heading: 'Meet the team',
  content:
    "COINCUBE was founded in 2014 to help cryptocurrency investors diversify their portfolios. We've been innovating ever since.",
  members: [
    {
      name: 'Robert Allen',
      title: 'Chief Executive Officer',
      biography:
        'A seasoned Python/Web App developer, Robert founded COINCUBE in early 2014. Prior to COINCUBE, he held FINRA Series 7 and 66 licenses which ultimately inspired him to create dynamic investment tools for the emerging digital asset classes.',
      githubUrl: 'https://github.com/robertwilliamallen',
      linkedinUrl: 'https://www.linkedin.com/in/robertwilliamallen/en',
      imageLocation: '../../static/img/team/robert.jpg',
    },
    {
      name: 'Eric Kittell',
      title: 'Developer',
      biography:
        "Eric started out coding Basic programs on the family Commodore 64 in the 1980s. He studied liberal arts at the University of Pennsylvania and started experimenting with websites in 1998. Since 2001 has worked as a freelance developer specializing in interactive database driven sites and web applications. Eric's most recent obsession with cryptocurrency trading has brought him into the COINCUBE project.",
      githubUrl: 'https://github.com/ekittell',
      linkedinUrl: 'https://www.linkedin.com/pub/eric-kittell/24/b58/897/en',
      imageLocation: '../../static/img/team/eric.jpg',
    },
    {
      name: 'Chris Barr',
      title: 'UI/UX Designer',
      biography:
        'Chris has over a decade of experience in design, branding, UI/UX, and digital product design. He studied at the Columbus College of Art and Design. He enjoys producing authentic digital products that customers love.',
      linkedinUrl: 'https://www.linkedin.com/in/cbarr/',
      imageLocation: '../../static/img/team/chrisbarr.png',
    },
    {
      name: 'Ben Wilson',
      title: 'DevOps',
      biography:
        'There is no userful dev without ops. Luckily, Ben can do both! He enjoys spending time automating as much of our infrastructure as possible, while tracking new projects and tech.',
      githubUrl: 'https://github.com/doubtingben',
      linkedinUrl: 'https://www.linkedin.com/in/ben-wilson-1261b66',
      imageLocation: '../../static/img/team/ben.jpg',
    },
    {
      name: 'Gabriel Nasr',
      title: 'DevOps',
      biography:
        'Gabriel is a curious, quick learner, interested in science and new technologies. He has experience in Linux System Administrator, troubleshooting, high availability web services, scripting (Python, bash), automation (Terraform, Ansible, Chef), containers (Kubernetes, Amazon ECS, Docker swarm), virtualization (AWS/KVM/OpenStack), monitoring (Sensu, Prometheus, Grafana, Telegraf, ELK), and web application and systems security',
      githubUrl: 'https://github.com/gnasr',
      linkedinUrl: 'https://www.linkedin.com/in/gabnasr/',
      imageLocation: '../../static/img/team/gabriel.jpg',
    },
  ],
};

export default content;
