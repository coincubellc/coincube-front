// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';

/* eslint-disable */
import '../build/_custom.scss';

// Absolute Imports
import axios from 'axios';
import DatatableFactory from 'vuejs-datatable';
import VeeValidate from 'vee-validate';
import VueAuth from '@websanova/vue-auth';
import VueAuthAxiosDriver from '@websanova/vue-auth/drivers/http/axios.1.x';
import VueAuthRouterDriver from '@websanova/vue-auth/drivers/router/vue-router.2.x';
import VueAxios from 'vue-axios';
import VueYouTubeEmbed from 'vue-youtube-embed';
import Toasted from 'vue-toasted';
import VueQriously from 'vue-qriously';
import Highcharts from 'highcharts/highstock';
import Clipboard from 'v-clipboard';
import VueFaqAccordion from 'vue-faq-accordion';

// Relative Imports
import App from './App';
import router from './router';
import VueAuthJwtDriver from './drivers/jwt';
import store from './store/store';
/* eslint-enable */

// Necessary to add Vue to the window due to DatatableFactory requirements
window.Vue = Vue;

Vue.config.productionTip = false;
Vue.router = router;

Vue.use(DatatableFactory);
Vue.use(VeeValidate);
Vue.use(VueAxios, axios);
Vue.use(VueYouTubeEmbed);
Vue.use(VueQriously);
Vue.use(Clipboard);

const root = `${window.env ? window.env.config.apiServer : ''}/api`;

Vue.use(Toasted, {
  duration: 5000,
  position: 'bottom-right',
  router,
});

Vue.use(VueAuth, {
  auth: VueAuthJwtDriver,
  http: VueAuthAxiosDriver,
  router: VueAuthRouterDriver,
  authType: 'jwt',
  parseUserData: data => data,
  // https://github.com/websanova/vue-auth/blob/master/docs/Options.md
  authRedirect: {
    path: '/?login=true',
  },
  forbiddenRedirect: {
    // path: '/403',
    path: '/?login=true',
  },
  notFoundRedirect: {
    path: '/404',
  },
  registerData: {
    url: `${root}/auth/register`,
    method: 'POST',
    redirect: null,
    fetchUser: false,
  },
  loginData: {
    url: `${root}/auth/login`,
    method: 'POST',
    redirect: null,
    fetchUser: false,
    rememberMe: false,
  },
  logoutData: {
    url: `${root}/auth/logout`,
    method: 'POST',
    redirect: '/',
    makeRequest: false,
  },
  fetchData: {
    url: `${root}/account/user`,
    method: 'GET',
    enabled: false,
  },
  refreshData: {
    url: `${root}/auth/refresh`,
    method: 'GET',
    enabled: true,
    interval: 30, // minutes
    isRefresh: true,
  },
});

/* eslint-disable-next-line no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});

Vue.toasted.register('error', message => message || 'null error message', {
  type: 'error',
});

Vue.toasted.register('info', message => message || 'null info message', {
  type: 'info',
});

Vue.toasted.register('success', message => message || 'null success message', {
  type: 'success',
});

Highcharts.setOptions({
  chart: {
    style: {
      fontFamily:
        'Roboto, "Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif',
    },
  },
});
