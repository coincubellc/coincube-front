import api from '@/utils/api';

const hello = require('hellojs/dist/hello.all.js');

hello.init(
  {
    facebook: '862174757218457',
    twitter: 'kQaHiQpR6NyFXR5duBLlB9jrA',
  },
  {
    redirect_uri: 'https://coincube.io/oauth/callback',
    display: 'popup',
  },
);

const loginFromOauth = (vueAuth, email, screenName, token, provider) =>
  api.auth.validateOauth(email, screenName, token, provider).then(res => {
    vueAuth.token(null, res.headers.authorization);
    // eslint-disable-next-line no-param-reassign
    vueAuth.watch.authenticated = true;
    // Required to persist the user across page refreshes
    document.cookie = 'rememberMe=false';
    hello.logout(provider);
    return res;
  });

export const facebook = vueAuth =>
  hello('facebook')
    .login({ scope: 'email' })
    .then(auth =>
      hello('facebook')
        .api('me', { scope: 'email' })
        .then(me => ({ me, auth })),
    )
    .then(({ auth, me }) =>
      loginFromOauth(
        vueAuth,
        me.email,
        me.first_name,
        auth.authResponse.access_token,
        'facebook',
      ),
    );

export const twitter = vueAuth =>
  hello('twitter')
    .login({ scope: 'email', include_email: 'true' })
    .then(auth =>
      hello('twitter')
        .api('me', { scope: 'email', include_email: 'true' })
        .then(me => ({ me, auth })),
    )
    .then(({ auth, me }) =>
      loginFromOauth(
        vueAuth,
        me.email,
        me.screen_name,
        auth.authResponse.access_token,
        'twitter',
      ),
    );
